<?php

class Queroeventos_Validate_UserName extends Zend_Validate_Abstract
{
    const STRING_EMPTY = 'stringEmpty';
    const NOT_NAME = 'notName';
    const NAME_TOO_SHORT = 'nameTooShort';

    protected $_messageTemplates = array(
        self::STRING_EMPTY => "please provide a name",
        self::NOT_NAME => "'%value%' is not a name",
    );

    public function isValid($value)
    {
        if(!is_string($value))
        {
            $this->_error(self::NOT_NAME);
            return false;
        }
        $this->_setValue((string) $value);

        if(strlen($value) < 3)
        {
            $this->_error(self::NAME_TOO_SHORT);
            return false;
        }
        
        return true;
    }
}