<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebMethodsUtil
 *
 * @author Daniel Cardoso
 */
class Queroeventos_WebMethodsUtil 
{
  
    const CIDADE=1;
    const ESTADO=2;
    const CATEGORIA=3;
   

   

    static public function  GerarCombo($TipoCombo)
    {
        switch ($TipoCombo)
        {
        case self::CIDADE:
            $tabelaCombo = new Model_Cidade_Table(Zend_Db_Table::getDefaultAdapter());            
            $combo = new Zend_Form_Element_Select('CID_ID');
            $combo->setLabel('Cidade:')
                  ->setRequired(true);
            $combo->addMultiOption('', 'Selecione...');
            foreach ($tabelaCombo->fetchAll(null, 'CID_TXT_NOME') as $row)
            {
                $combo->addMultiOption($row->getId(), $row->getNome());
            }
            break;
        case self::CATEGORIA:
            $tabelaCombo = new Model_Categoria_Table(Zend_Db_Table::getDefaultAdapter());           
            $combo = new Zend_Form_Element_Select('CTG_ID');
            $combo->setLabel('Categoria:')
                  ->setRequired(true);
            $combo->addMultiOption('', 'Selecione...');
            foreach ($tabelaCombo->fetchAll(null, 'CTG_TXT_NOME') as $row)
            {
                $combo->addMultiOption($row->getId(), $row->getNome());
            }
            break;
         case self::ESTADO: 
            $tabelaCombo = new Model_Estado_Table(Zend_Db_Table::getDefaultAdapter());            
            $combo = new Zend_Form_Element_Select('EST_UF');
            $combo->setLabel('Estado:')
                  ->setRequired(true);
            $combo->addMultiOption('', 'Selecione...');
            foreach ($tabelaCombo->fetchAll(null, 'EST_TXT_NOME') as $row)
            {
                $combo->addMultiOption($row->getUf(), $row->getNome());
            }
            break;
        }
        return $combo;
    }
}
