<?php

class Queroeventos_Form_Element_Name extends Zend_Form_Element_Xhtml
{
    public $helper = 'nameElement';
    
    protected $_name_value = '';

    private function setValueName($value)
    {
        $this->_name_value = $value;
        return $this;
    }

    public function  setValue($value)
    {
        if(!empty ($value))
        {
            $this->setValueName($value);
        }
    }

    public function  getValue()
    {
        if(empty ($this->_name_value))
        {
            return false;
        }
        else
        {
            return $this->_name_value;
        }
    }
}