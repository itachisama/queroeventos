<?php

class Queroeventos_View_Helper_NameElement extends Zend_View_Helper_FormElement
{
    protected $_html = '';

    public function nameElement($name, $value = null, $atribs = null)
    {
        $username = '';

        if($value)
        {
            //$username = $value->username;
        }
        $helper = new Zend_View_Helper_FormText();
        $helper->setView($this->view);

        $this->_html .= $helper->formText($name . '[username]', $username, array());

        return $this->_html;
    }
}