<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DTO
 *
 * @author Daniel Cardoso
 */
class APP_DTO_Usuario {

   public $_id;
   public $_idConta;
   public $_idCidade;
   public $_nome;
   public $_email;
   public $_webPage;
   public $_facebook;
   public $_twitter;
   public $_sexo;
   public $_dataCadastro;
   public $_dataNascimento;
}

