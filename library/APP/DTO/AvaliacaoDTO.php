<?php

/**
 * Description of Avaliacao
 *
 * @author Daniel
 */
class APP_DTO_Avaliacao
{
    
    public $_id;
    public $_idUsuario;
    public $_idEvento;
    public $_nota;
    public $_comentario;
}