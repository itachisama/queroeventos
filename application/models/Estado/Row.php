<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Row
 *
 * @author Daniel Cardoso
 */
class Model_Estado_Row extends Zend_Db_Table_Row_Abstract
{

    public function __toString()
    {

        return (string) $this->EST_TXT_NOME;

    }

    public function getNome()
    {

        return (string) $this->EST_TXT_NOME;

    }

    public function getUf()
    {

        return (string) $this->EST_UF;

    }
}
