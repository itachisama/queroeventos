<?php
/**
 * Description of Table
 *
 * @author Daniel Cardoso
 */
class Model_Estado_Table extends Zend_Db_Table_Abstract
{
    protected $_name = "TB_ESTADO";

    protected $_primary = "EST_UF";

    protected $_rowClass = 'Model_Estado_Row';

    /**
     * Tabelas que referenciam estado.
     * 
     * @var array  
     */
    protected $_dependentTables = array('Model_Cidade_Table');
}
