<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Table
 *
 * @author Daniel Cardoso
 */
class Model_Perfil_Table extends Zend_Db_Table_Abstract
{
    protected $_name = "TB_PERFIL";

    protected $_primary = "PRF_ID";

    protected $_rowClass = 'Default_Model_Perfil_Row';
}
