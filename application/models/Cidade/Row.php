<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Row
 *
 * @author Daniel Cardoso
 */
class Model_Cidade_Row extends Zend_Db_Table_Row_Abstract
{

    public function __toString()
    {

        return (string) $this->CID_TXT_NOME;

    }
    
    public function getId()
    {

        return (string) $this->CID_ID;
    }

     public function getNome()
    {

        return (string) $this->CID_TXT_NOME;

    }
}
