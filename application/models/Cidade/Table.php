<?php
/**
 * Description of Table
 *
 * @author Daniel Cardoso
 */
class  Model_Cidade_Table extends Zend_Db_Table_Abstract
{
    protected $_name = "TB_CIDADE";

    protected $_primary = "CID_ID";

    protected $_rowClass = 'Model_Cidade_Row';

    /**
     * mapeamento de cidade/estado
     * 
     * @var array
     */
    protected $_referenceMap = array(
        'estado'    =>  array(
            'columns'       =>  'EST_UF',
            'refTableClass' =>  'Model_Estado_Table',
            'refColumns'    =>  'EST_UF',
        ),
    );
}
