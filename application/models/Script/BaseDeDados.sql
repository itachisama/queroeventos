/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     27/03/2011 00:03:10                          */
/*==============================================================*/


drop table if exists TB_AVALIACAO_EVT;

drop table if exists TB_CATEGORIA;

drop table if exists TB_CIDADE;

drop table if exists TB_CONTA;

drop table if exists TB_ESTADO;

drop table if exists TB_EVENTO;

drop table if exists TB_HISTORICO_EVENTO;

drop table if exists TB_LISTA_CONTATO;

drop table if exists TB_PARTICIPA_EVENTO;

drop table if exists TB_PERFIL;

drop table if exists TB_TAGS;

drop table if exists TB_TAG_EVENTO;

drop table if exists TB_USUARIO;

drop table if exists TB_USU_PERTENCE_LISTA;

/*==============================================================*/
/* Table: TB_AVALIACAO_EVT                                      */
/*==============================================================*/
create table TB_AVALIACAO_EVT
(
   AVT_ID               int not null AUTO_INCREMENT,
   USU_ID               int not null,
   EVT_ID               int not null,
   AVT_NUM_NOTA         decimal(2,1) not null,
   AVT_TXT_COMENTARIO   varchar(200) not null,
   primary key (AVT_ID)
);

/*==============================================================*/
/* Table: TB_CATEGORIA                                          */
/*==============================================================*/
create table TB_CATEGORIA
(
   CTG_ID               int not null AUTO_INCREMENT,
   CTG_TXT_NOME         varchar(30) not null,
   primary key (CTG_ID)
);

/*==============================================================*/
/* Table: TB_CIDADE                                             */
/*==============================================================*/
create table TB_CIDADE
(
   CID_ID               int not null,
   EST_UF               char(2) not null,
   CID_TXT_NOME         varchar(30) not null,
   primary key (CID_ID)
);

/*==============================================================*/
/* Table: TB_CONTA                                              */
/*==============================================================*/
create table TB_CONTA
(
   USU_ID               int not null,
   CNT_TXT_LOGIN        varchar(20) not null,
   PRF_ID               int not null,
   CNT_TXT_SENHA        varchar(32) not null,
   primary key (USU_ID, CNT_TXT_LOGIN)
);

/*==============================================================*/
/* Table: TB_ESTADO                                             */
/*==============================================================*/
create table TB_ESTADO
(
   EST_UF               char(2) not null,
   EST_TXT_NOME         varchar(30) not null,
   primary key (EST_UF)
);

/*==============================================================*/
/* Table: TB_EVENTO                                             */
/*==============================================================*/
create table TB_EVENTO
(
   EVT_ID               int not null AUTO_INCREMENT,
   CID_ID               int not null,
   USU_ID               int,
   CTG_ID               int not null,
   EVT_DT_INICIO        datetime not null,
   EVT_DT_FIM           datetime not null,
   EVT_TXT_NOME         varchar(50) not null,
   EVT_TXT_DESCRICAO    varchar(500) not null,
   EVT_FLG_TIPO         char(1) not null,
   EVT_FLG_STATUS       varchar(20) not null,
   EVT_TXT_ENDERECO     varchar(50) not null,
   EVT_FLG_MAIORIDADE   char(1),
   EVT_IMG_ARQUIVO      longblob,
   EVT_IMG_TIPO         varchar(3),
   primary key (EVT_ID)
);

/*==============================================================*/
/* Table: TB_HISTORICO_EVENTO                                   */
/*==============================================================*/
create table TB_HISTORICO_EVENTO
(
   HEV_ID               int not null AUTO_INCREMENT,
   USU_ID               int not null,
   EVT_ID               int not null,
   HEV_TXT_DESCRICAO    varchar(500) not null,
   EVT_FLG_STATUS       varchar(20) not null,
   HEV_DT_ALTERACAO     datetime not null,
   primary key (HEV_ID)
);

/*==============================================================*/
/* Table: TB_LISTA_CONTATO                                      */
/*==============================================================*/
create table TB_LISTA_CONTATO
(
   LTC_ID               int not null AUTO_INCREMENT,
   USU_ID               int not null,
   LTC_NOME_LISTA       char(50) not null,
   LTC_PRIVADA          char(1),
   primary key (LTC_ID)
);

/*==============================================================*/
/* Table: TB_PARTICIPA_EVENTO                                   */
/*==============================================================*/
create table TB_PARTICIPA_EVENTO
(
   USU_ID               int not null,
   EVT_ID               int not null,
   primary key (USU_ID, EVT_ID)
);

/*==============================================================*/
/* Table: TB_PERFIL                                             */
/*==============================================================*/
create table TB_PERFIL
(
   PRF_ID               int not null AUTO_INCREMENT,
   PRF_TXT_NOME         varchar(15) not null,
   primary key (PRF_ID)
);

/*==============================================================*/
/* Table: TB_TAGS                                               */
/*==============================================================*/
create table TB_TAGS
(
   TAG_ID               int not null AUTO_INCREMENT,
   TAG_TXT_NOME         varchar(20) not null,
   primary key (TAG_ID)
);

/*==============================================================*/
/* Table: TB_TAG_EVENTO                                         */
/*==============================================================*/
create table TB_TAG_EVENTO
(
   TAG_ID               int not null,
   EVT_ID               int not null,
   primary key (TAG_ID, EVT_ID)
);

/*==============================================================*/
/* Table: TB_USUARIO                                            */
/*==============================================================*/
create table TB_USUARIO
(
   USU_ID               int not null AUTO_INCREMENT,
   CID_ID               int not null,
   USU_TXT_NOME         varchar(50) not null,
   USU_TXT_EMAIL        varchar(30) not null,
   USU_TXT_WEB_PAGE     varchar(50),
   USU_DT_NASCIMENTO    date not null,
   USU_FLG_SEXO         char(1) not null,
   USU_DT_CADASTRO      datetime not null,
   USU_FLG_TWITTER      char(1),
   USU_FLG_FACEBOOK     char(1),
   USU_IMG_ARQUIVO      longblob,
   USU_IMG_TIPO         varchar(3),
   primary key (USU_ID)
)


/*==============================================================*/
/* Table: TB_USU_PERTENCE_LISTA                                 */
/*==============================================================*/
create table TB_USU_PERTENCE_LISTA
(
   LTC_ID               int not null,
   USU_ID               int not null,
   primary key (LTC_ID, USU_ID)
);

alter table TB_AVALIACAO_EVT add constraint FK_AVALIACAO_EVENTO foreign key (EVT_ID)
      references TB_EVENTO (EVT_ID) on delete restrict on update restrict;

alter table TB_AVALIACAO_EVT add constraint FK_EVENTO_USUARIO foreign key (USU_ID)
      references TB_USUARIO (USU_ID) on delete restrict on update restrict;

alter table TB_CIDADE add constraint FK_ESTADO_CIDADE foreign key (EST_UF)
      references TB_ESTADO (EST_UF) on delete restrict on update restrict;

alter table TB_CONTA add constraint FK_PERFIL_CONTA foreign key (PRF_ID)
      references TB_PERFIL (PRF_ID) on delete restrict on update restrict;

alter table TB_CONTA add constraint FK_USUARIO_CONTA foreign key (USU_ID)
      references TB_USUARIO (USU_ID) on delete restrict on update restrict;

alter table TB_EVENTO add constraint FK_CATEGORIA_EVENTO foreign key (CTG_ID)
      references TB_CATEGORIA (CTG_ID) on delete restrict on update restrict;

alter table TB_EVENTO add constraint FK_EVENTO_CIDADE foreign key (CID_ID)
      references TB_CIDADE (CID_ID) on delete restrict on update restrict;

alter table TB_EVENTO add constraint FK_USU_CRIA_EVENTO foreign key (USU_ID)
      references TB_USUARIO (USU_ID) on delete restrict on update restrict;

alter table TB_HISTORICO_EVENTO add constraint FK_HISTORICO_EVT_ID foreign key (EVT_ID)
      references TB_EVENTO (EVT_ID) on delete restrict on update restrict;

alter table TB_HISTORICO_EVENTO add constraint FK_HISTORICO_USU_ID foreign key (USU_ID)
      references TB_USUARIO (USU_ID) on delete restrict on update restrict;

alter table TB_LISTA_CONTATO add constraint FK_USU_POSSUI_LISTA foreign key (USU_ID)
      references TB_USUARIO (USU_ID) on delete restrict on update restrict;

alter table TB_PARTICIPA_EVENTO add constraint FK_PARTICIPA_EVENTO foreign key (USU_ID)
      references TB_USUARIO (USU_ID) on delete restrict on update restrict;

alter table TB_PARTICIPA_EVENTO add constraint FK_PARTICIPA_EVENTO2 foreign key (EVT_ID)
      references TB_EVENTO (EVT_ID) on delete restrict on update restrict;

alter table TB_TAG_EVENTO add constraint FK_TAG_EVENTO foreign key (TAG_ID)
      references TB_TAGS (TAG_ID) on delete restrict on update restrict;

alter table TB_TAG_EVENTO add constraint FK_TAG_EVENTO2 foreign key (EVT_ID)
      references TB_EVENTO (EVT_ID) on delete restrict on update restrict;

alter table TB_USUARIO add constraint FK_USUARIO_CIDADE foreign key (CID_ID)
      references TB_CIDADE (CID_ID) on delete restrict on update restrict;

alter table TB_USU_PERTENCE_LISTA add constraint FK_USU_PERTENCE_LISTA foreign key (LTC_ID)
      references TB_LISTA_CONTATO (LTC_ID) on delete restrict on update restrict;

alter table TB_USU_PERTENCE_LISTA add constraint FK_USU_PERTENCE_LISTA2 foreign key (USU_ID)
      references TB_USUARIO (USU_ID) on delete restrict on update restrict;

