<?php

class Model_Tag_Table extends Zend_Db_Table_Abstract
{
    protected $_name = "TB_TAGS";

    protected $_primary = "TAG_ID";

    protected $_rowClass = 'Model_Tag_Row';
}