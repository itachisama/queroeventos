<?php

class Model_Avaliacao_Table extends Zend_Db_Table_Abstract
{
    protected $_name = "TB_AVALIACAO_EVENTO";

    protected $_primary = "AVT_ID";

    protected $_rowClass = 'Default_Model_Avaliacao_Row';
}