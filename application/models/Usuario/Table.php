<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Table
 *
 * @author Daniel Cardoso
 */
class Model_Usuario_Table extends Zend_Db_Table_Abstract
{
    protected $_name = "TB_USUARIO";

    protected $_primary = "USU_ID";

    protected $_rowClass = 'Default_Model_Usuario_Row';
}
