<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Table
 *
 * @author Daniel Cardoso
 */
class Model_Conta_Table extends Zend_Db_Table_Abstract
{
    protected $_name = "TB_CONTA";

    protected $_primary = "CNT_ID";

    protected $_rowClass = 'Default_Model_Conta_Row';
}
