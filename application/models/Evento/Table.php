<?php

class Model_Evento_Table extends Zend_Db_Table_Abstract
{
    protected $_name = "TB_EVENTO";

    protected $_primary = "EVT_ID";

    protected $_rowClass = 'Model_Evento_Row';
}