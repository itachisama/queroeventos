<?php

class Model_Categoria_Row extends Zend_Db_Table_Row_Abstract
{

    public function __toString()
    {

        return (string) $this->CTG_TXT_NOME;

    }

     public function getNome()
    {

        return (string) $this->CTG_TXT_NOME;

    }

     public function getID()
    {

        return (string) $this->CTG_ID;

    }
}