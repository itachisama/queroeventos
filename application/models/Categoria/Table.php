<?php

class Model_Categoria_Table extends Zend_Db_Table_Abstract
{
    protected $_name = "TB_CATEGORIA";

    protected $_primary = "CTG_ID";

    protected $_rowClass = 'Model_Categoria_Row';
}