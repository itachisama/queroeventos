<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categoria
 *
 * @author Daniel Cardoso
 */
class Form_Categoria extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');

        $this->addElement('text', 'nome_ctg', array(
                'label'         => 'Nome da categoria:',
                'required'      => true,
                    )
                );        
        
        $this->addElement('submit', 'submit', array(
                'ignore'    => true,
                'label'     => 'Cadastrar Categoria',
            )
        );
    }
}

