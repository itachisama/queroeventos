<?php

class Form_Evento extends Zend_Form
{
    public function init()
    {
        ZendX_Jquery::enableForm($this);

        $this->setMethod('post');

        $this->addElement('text', 'nome_evento', array(
                'label'         => 'Nome do evento:',
                'required'      => true,
                    )
                );

        $date = new Zend_Date();

        $data_inicio = new ZendX_JQuery_Form_Element_DatePicker(
                'data_inicio',
                array('jQueryParams' => array(
                    'defaultDate'    => $date->get(Zend_Date::DATE_MEDIUM, 'pt_BR'),
                    'dateFormat'     => 'dd/mm/yy',
                ))
                );

        $data_inicio->setLabel('Data de inicio do evento:');
        $this->addElement($data_inicio);
        $horaInicio = new Zend_Form_Element_Text('hora_inicio');
        $horaInicio ->setLabel('Hora do Inicio')
                ->setRequired(true)
                ->setAttribs(array('onchange'=>'verifica_horas(this)','onkeypress'=>'valida_horas(this, event)',
                    'size'=>'3', 'maxlength'=>'5'));

        $this->addElement($horaInicio);

        $data_fim = new ZendX_JQuery_Form_Element_DatePicker(
                'data_fim',
                array('jQueryParams' => array(
                    'defaultDate'    => $date->get(Zend_Date::DATE_MEDIUM, 'pt_BR'),
                    'dateFormat'     => 'dd/mm/yy',
                ))
                );

        $data_fim->setLabel('Data de finalização do evento:');

        $this->addElement($data_fim);
        $horaFim = new Zend_Form_Element_Text('hora_fim');
        $horaFim ->setLabel('Hora do Termino')
                ->setRequired(true)                
                ->setAttribs(array('onchange'=>'verifica_horas(this)','onkeypress'=>'valida_horas(this, event)',
                    'size'=>'3', 'maxlength'=>'5'));

        $this->addElement($horaFim);
        $this->addElement('textarea', 'descricao_evento', array(
                'label'         => 'Descrição do evento:',
                'required'      => true,
                    )
                );

        $estadosSelect = Queroeventos_WebMethodsUtil::GerarCombo(Queroeventos_WebMethodsUtil::ESTADO,false,0);
        $estadosSelect->setAttribs(array(
                          'onchange'    =>  'buscaCidades(this.value)',
                      ));

        $this->addElement($estadosSelect);

        $cidadesSelect = new Zend_Form_Element_Select('cidade');
        $cidadesSelect->setLabel('Cidade:')
                      ->setRequired(true)
                      ->addValidators(array(
                        array('NotEmpty'),
                      ));
        $cidadesSelect->addMultiOption('0', 'Selecione...');

        $this->addElement($cidadesSelect);

        $this->addElement('text', 'endereco_evento', array(
                'label'         => 'Local do evento:',
                'required'      => true,
                    )
                );

        $this->addElement(Queroeventos_WebMethodsUtil::GerarCombo(Queroeventos_WebMethodsUtil::CATEGORIA));
        $radio_privado = new Zend_Form_Element_Radio('evento_privado');
        $radio_privado->setLabel('Evento privado?')
                   ->addMultiOptions(array(
                       'S'  => 'Sim',
                       'N'   => 'Não',
                   ))
                   ->setSeparator('');

        $this->addElement($radio_privado);

        $radio_maioridade = new Zend_Form_Element_Radio('evento_maioridade');
        $radio_maioridade->setLabel('Evento para Adultos ?')
                   ->addMultiOptions(array(
                       'S'  => 'Sim',
                       'N'   => 'Não',
                   ))
                   ->setSeparator('');

        $this->addElement($radio_maioridade);

        $this->addElement('submit', 'submit', array(
                'ignore'    => true,
                'label'     => 'Enviar',
            )
        );
    }
}