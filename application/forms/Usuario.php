<?php

class Form_Usuario extends Zend_Form
{
    public function init()
    {
        ZendX_Jquery::enableForm($this);

        $this->setMethod('post');

        //$username = new Queroeventos_Form_Element_Name('username');
        //$username->addValidator(new Queroeventos_Validate_UserName());
        //$username->setLabel('Nome:')->setRequired(true);
        $this->addElement('text', 'nome_usuario', array(
                'label'         =>  'Nome:',
                'required'      =>  true,
                    )
                );

         $this-> addElement('text', 'email_usuario', array(
                'label'         => 'Email:',
                'required'      => true,
                    )
                );

        $this->addElement('text', 'webpage_usuario', array(
                'label'         => 'Página web:',
                'required'      => false,
                    )
                );
       
        $date = new Zend_Date();

        $dataNascimento = new ZendX_JQuery_Form_Element_DatePicker(
                'data_nascimento',
                array('jQueryParams' => array(
                    'defaultDate'    => $date->get(Zend_Date::DATE_MEDIUM, 'pt_BR'),
                    'dateFormat'     => 'dd/mm/yy',
                ))
                );
        $dataNascimento->setRequired(false);

        $dataNascimento->setLabel('Data de Nascimento:');

        $this->addElement($dataNascimento);

//        $estadosSelect = new Zend_Form_Element_Select('UF');
//        $estadosSelect->setLabel('Estado:')
//                      ->setRequired(true)
//                      ->addValidators(array(
//                        array('NotEmpty'),
//                      ))
//                      ->setAttribs(array(
//                          'onchange'    =>  'buscaCidades(this.value)',
//                      ));
//        $estadosSelect->addMultiOption('0', 'Selecione...');
//        $estadosSelect->addMultiOption('RJ', 'Rio de Janeiro');
//
//        $this->addElement($estadosSelect);

        $estadosSelect = Queroeventos_WebMethodsUtil::GerarCombo(Queroeventos_WebMethodsUtil::ESTADO);
        $estadosSelect->setAttribs(array(
                          'onchange'    =>  'buscaCidades(this.value)',
                      ));
        
        $this->addElement($estadosSelect);

        $cidadesSelect = new Zend_Form_Element_Select('cidade');
        $cidadesSelect->setLabel('Cidade:')
                      ->setRequired(true)
                      ->addValidators(array(
                        array('NotEmpty'),
                      ));
        $cidadesSelect->addMultiOption('0', 'Selecione...');

        $this->addElement($cidadesSelect);

        $this->addElement('checkbox', 'twitter', array(
                'label'         => 'Twitter',
                'required'      => false,
                    )
                );

        $this->addElement('checkbox', 'facebook', array(
                'label'         => 'Facebook',
                'required'      => false,
                    )
                );

        $radio_sexo = new Zend_Form_Element_Radio('sexo');
        $radio_sexo->setLabel('Sexo:')
                   ->addMultiOptions(array(
                       'masculino'  => 'Masculino',
                       'feminino'   => 'Feminino',
                   ))
                   ->setSeparator('');

        $this->addElement($radio_sexo);

        $this->addElement('submit', 'submit', array(
                'ignore'    => true,
                'label'     => 'Enviar',
            )
        );
    }
}