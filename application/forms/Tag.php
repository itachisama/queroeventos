<?php

class Form_Tag extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');

        $this->addElement('text', 'nome_tag', array(
                'label'         => 'Nome da tag:',
                'required'      => true,
                    )
                );

        $this->addElement('submit', 'submit', array(
                'ignore'    => true,
                'label'     => 'Cadastrar Tag',
            )
        );
    }
}