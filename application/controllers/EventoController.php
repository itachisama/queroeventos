<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventoController
 *
 * @author Daniel Cardoso
 */
class EventoController extends Zend_Controller_Action
{
    /**
     * Adiciona regras de acl para as actions desde controller
     */
    public function init()
    {
        parent::init();
        $this->_helper->_acl->allow(null);
    }

    /**
     * Action padrão
     */
    public function indexAction()
    {

    }

     public function recuperacidadesAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->getHelper('layout')->disableLayout();

        $modelCidade = new Model_Cidade_Table();
        $ufEstado = (string) $this->_request->getParam('EST_UF', '');
        $retornoJson = array();

        $rowCidades = $modelCidade->fetchAll(array('EST_UF = ?' => $ufEstado));

        if(count($rowCidades) < 0)
        {
            print "0";
        }
        else
        {
            header('Content-type: application/json');
            print Zend_Json::encode($rowCidades->toArray());
        }
    }

     public function validateformAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->getHelper('layout')->disableLayout();

        $f = new Form_Evento();
        $f->isValid($this->_getAllParams());
        $json = $f->getMessages();
        header('Content-type: application/json');
        echo Zend_Json::encode($json);
    }

    public function cadastrarAction()
    {
        $request = $this->getRequest();
        $form = new Form_Evento();
        $mensagem = '';
       
        if($request->isPost())
        {
            if($form->isValid($request->getPost()))
            {
                $dados_post = $request->getPost();
                $statusEvento = 'ATV';
                $dataInicio = new DateTime(array($dados_post['data_inicio'],$dados_post['hora_inicio']));
                $dataFim = new DateTime(array($dados_post['data_fim'],$dados_post['hora_fim']));
                $EventoObj =array(
                    'CID_ID'=>$dados_post['cidade'],
                    'USU_ID'=>$dados_post['nome_evento'],
                    'CTG_ID'=>$dados_post['categoria'],
                    'EVT_DT_INICIO'=>$dataInicio,
                    'EVT_DT_FIM'=>$dataFim,
                    'EVT_TXT_NOME'=>$dados_post['nome_evento'],
                    'EVT_TXT_DESCRICAO'=>$dados_post['descricao_evento'],
                    'EVT_FLG_STATUS'=>$statusEvento,
                    'EVT_TXT_ENDERECO'=>$dados_post['endereco_evento'],
                    'EVT_FLG_MAIORIDADE'=>$dados_post['evento_maioridade'],
                    'EVT_IMG_ARQUIVO'=>$dados_post['nome_evento']
                );
                
               $evento = new Model_Evento_Table();

                try{

                    $evento->insert($EventoObj);
                    $mensagem = 'Dados cadastrados com sucesso!';
                    $this->view->mensagem = $mensagem;

                    return $this->_helper->redirector('cadastrar');

                } catch (Exception $e)
                {
                    $mensagem = $e->getMessage();
                }
            }
        }

        $this->view->form = $form;
    }
}