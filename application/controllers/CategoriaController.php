<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoriaController
 *
 * @author Daniel Cardoso
 */
class CategoriaController extends Zend_Controller_Action{
    /**
     * Adiciona regras de acl para as actions desde controller
     */
    public function init()
    {
        parent::init();
        $this->_helper->_acl->allow(null);
    }

    /**
     * Action padrão
     */
    public function indexAction()
    {

    }

    public function cadastrarAction()
    {
        $request = $this->getRequest();
        $form = new Form_Categoria();
        $mensagem = '';

        if($request->isPost())
        {
            if($form->isValid($request->getPost()))
            {
                $dados_post = $request->getPost();
                $nome_tag = $dados_post['nome_ctg'];               
                $dados = array(
                    'CTG_TXT_NOME' =>  $nome_tag,
                );

                $tag = new Model_Categoria_Table();

                try{

                    $tag->insert($dados);
                    $mensagem = 'Dados cadastrados com sucesso!';
                    $this->view->mensagem = $mensagem;

                    return $this->_helper->redirector('cadastrar');

                } catch (Exception $e)
                {
                    $mensagem = $e->getMessage();
                }
            }
        }

        $this->view->form = $form;
    }
}

