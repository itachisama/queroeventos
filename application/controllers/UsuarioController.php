<?php

class UsuarioController extends Zend_Controller_Action
{
    private $_formUsuario = null;

    /**
     * Adiciona regras de acl para as actions desde controller
     */
    public function init()
    {
        parent::init();
        $this->_helper->_acl->allow(null);
    }

    /**
     * Action padrão
     */
    public function indexAction()
    {
        
    }

    public function recuperacidadesAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->getHelper('layout')->disableLayout();
        
        $modelCidade = new Model_Cidade_Table();
        $ufEstado = (string) $this->_request->getParam('EST_UF', '');
        $retornoJson = array();
        
        $rowCidades = $modelCidade->fetchAll(array('EST_UF = ?' => $ufEstado));

        if(count($rowCidades) < 0)
        {
            print "0";
        }
        else
        {
            header('Content-type: application/json');
            print Zend_Json::encode($rowCidades->toArray());
        }
    }

    public function cadastrarAction()
    {
        $request = $this->getRequest();
        $this->_formUsuario = new Form_Usuario();
        $mensagem = '';

        if($request->isPost())
        {
            if($this->_formUsuario->isValid($request->getPost()))
            {
                //TODO: adicionar rotina de cadastro...
            }
        }
        $this->view->setEncoding('utf-8');
        $this->view->form = $this->_formUsuario;
    }


    public function validateformAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->getHelper('layout')->disableLayout();

        $f = new Form_Usuario();
        $f->isValid($this->_getAllParams());
        $json = $f->getMessages();
        header('Content-type: application/json');
        echo Zend_Json::encode($json);
    }

    public function loginAction()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->messages = $this->_flashMessenger->getMessages();
        $form = new Form_Login();
        $this->view->form = $form;
        //Verifica se existem dados de POST
        if ( $this->getRequest()->isPost() ) {
            $data = $this->getRequest()->getPost();
            //Formulário corretamente preenchido?
            if ( $form->isValid($data) ) {
                $login = $form->getValue('login');
                $senha = $form->getValue('senha');

                $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                //Inicia o adaptador Zend_Auth para banco de dados
                $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
                $authAdapter->setTableName('TB_CONTA')
                            ->setIdentityColumn('CNT_TXT_LOGIN')
                            ->setCredentialColumn('CNT_TXT_SENHA');
                            //TODO: Descomentar a linha quando ja usar o padrão MD5
                            //->setCredentialTreatment('SHA1(?)');
                //Define os dados para processar o login
                $authAdapter->setIdentity($login)
                            ->setCredential($senha);
                //Efetua o login
                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authAdapter);
                //Verifica se o login foi efetuado com sucesso
                if ( $result->isValid() ) {
                    //Armazena os dados do usuário em sessão, apenas desconsiderando
                    //a senha do usuário
                    $info = $authAdapter->getResultRowObject(null, 'CNT_TXT_SENHA');
                    $storage = $auth->getStorage();
                    $storage->write($info);                   
                } else {
                    //Dados inválidos
                    $this->_helper->FlashMessenger('Usuário ou senha inválidos!');
                    $this->_redirect('/usuario/login');
                }
            } else {
                //Formulário preenchido de forma incorreta
                $form->populate($data);
            }
        }
    }
}